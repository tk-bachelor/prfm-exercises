-- Lecture 14

nums :: [Int]
nums = [1,3,5,2,6,7,8]

inc [] = []
inc (x:xs) = x+1 : inc xs

inc2 = map (+1)

-- Maybe
-- data Maybe x = Just x | Nothing

mnums :: [Maybe Int]
mnums = [Just 1, Just 2, Just 3, Nothing]

minc :: Maybe Int -> Maybe Int
minc Nothing = Nothing
minc (Just x) = Just (x+1)

-- Functor
inc3 :: Functor f => f Int -> f Int
inc3 = fmap (+1)

inc4 :: Functor f => f (Maybe Int) -> f (Maybe Int)
inc4 = fmap minc

-- Exercises

-- Ex 1.

data Tree a = Leaf a | Node (Tree a) a (Tree a) deriving Show

instance Functor Tree where
    fmap f (Leaf a) = Leaf (f a)
    fmap f (Node l x r) = Node (fmap f l) (f x) (fmap f r)

tr = [Leaf 1, Leaf 2]

-- Ex 7.

data Expr a = Val a | Add (Expr a) (Expr a) deriving Show

-- Functor
instance Functor Expr where
    fmap f (Val v) = Val (f v)
    fmap f (Add l r) = Add (fmap f l) (fmap f r)

-- Applicative
instance Applicative Expr where
    pure = Val
    Val n <*> f = fmap n f

-- Monad
instance Monad Expr where
    (Val v) >>= f = f v
