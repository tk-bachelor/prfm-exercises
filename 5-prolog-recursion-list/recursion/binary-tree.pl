swap(leaf(X), leaf(X)).
swap(tree(B1, B2), tree(B2Swapped, B1Swapped)) :-
    swap(B1, B1Swapped),
    swap(B2, B2Swapped).

% swap(tree(leaf(1), leaf(2)), T).
% tree(B1, B2) --> B1 = leaf(1), B2 --> leaf(2)
% 				swap(leaf(1), B1Swapped),
% 				swap(leaf(2), B2Swapped).
% 				
% swap(leaf(X), leaf(X)) --> X = 1, B1Swapped = leaf(1)
% 							 X = 2, B2Swapped = leaf(2)
% 							 
% T = tree(B2Swapped, B1Swapped)
% 	= tree(leaf(2), leaf(1))

