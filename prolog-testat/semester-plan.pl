module(oop).
module(an1).
module(bsys).
module(parprog).
module(se1).
module(se2).
module(an2).
module(sa).

ects(oop, 6).
ects(an1, 4).
ects(bsys, 4).
ects(parprog, 4).
ects(se1, 4).
ects(se2, 4).
ects(an2, 4).
ects(sa, 8).

% module X requires a list of modules as prerequisites and minimum ECTS modules
require(oop, [], 0).
require(an1, [], 0).
require(bsys, [], 0).
require(se1, [oop], 0).
require(se2, [se1], 0).
require(an2, [an1], 0).
require(parprog, [oop, bsys], 0).
require(sa, [se2, bsys, an1], 20). % SA requires min. 20 ECTS

% Question 1: find all required modules (direct and indirect dependencies) by mutual recursion
% Query: dependent(se2, X)
% Result: X = [se1, oop]
dependent(Module,Dep):- module(Module), require(Module, DirectDep, _), findAllDep(DirectDep,Dep).

findAllDep([],[]).
findAllDep([H|T], Dep) :- dependent(H,HDep), findAllDep(T,TDep), append([H|HDep], TDep, Dep).

% passed modules as list
%passed([]).
passed([oop, an1, bsys]).
not_passed(Module) :- module(Module), passed(Passed), not_member(Module, Passed).

% passed modules as single predicates
% passed(null).
% passed(oop).
% not_passed(Module) :- module(Module), passed(Passed), Passed \= null, Module \= Passed.

not_member(_, []).
not_member(X, [H|T]) :- X \= H, not_member(X, T).

% Question 2: Which modules can I enroll next?
% Query: canEnroll(X).
% Result: X = [parprog, se1, an2]
% canEnroll(Module) :- passed(null), require(Module, []).
canEnroll(Module) :- achievedEcts(AE), canEnroll(Module, AE).
canEnroll(Module, AE) :- module(Module), not_passed(Module), 
    require(Module, DirectDep, E), isEnough(AE, E),
    passed(Passed), subset(DirectDep, Passed).
    
% Question 3: How many ECTS Points have I already achieved?
% Query: achievedEcts(X)
% Result: 14
achievedEcts(X) :- passed(Passed), achievedEcts(Passed, X).
achievedEcts([], 0).
achievedEcts([M|T], E) :- achievedEcts(T, TE), ects(M, ME), E is TE + ME.

isEnough(Achieved, RequiredEcts) :- Achieved >= RequiredEcts.