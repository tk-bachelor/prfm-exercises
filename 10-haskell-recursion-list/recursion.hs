sumdown :: Int -> Int
sumdown 0 = 0
sumdown x = x + sumdown (x-1)

power :: Int -> Int -> Int
power b 0 = 1
power b e = b * power b (e-1)

euclid x y | x == y = x
           | x > y = euclid (x-y) y 
           | x < y = euclid x (y-x)

-- Ex 6
and2 [] = True
and2 (x:xs) | x == True = and2 xs
            | otherwise = False

concat2 [] = []
concat2 (xs:xss) = xs ++ concat2 xss

nth (x:_) 0 = x
nth (_:xs) n = nth xs (n-1)

contains e [] = False
contains e (x:xs) | e == x = True
                  | otherwise = contains e xs

merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys) | x <= y = x : merge xs (y:ys)
                    | otherwise = y : merge (x:xs) ys