-- Live demo of functional programming in Haskell
-- Demonstration of
--   * Use of GHCi
--   * Function definition
--   * Lazy evaluation
--   * Abstract data types
--   * Higher order functions
--   * Anonymous functons
--   * Type inferrence
--
-- Farhad Mehta

-- Guide to using GHCi
-- https://downloads.haskell.org/~ghc/7.4.1/docs/html/users_guide/ghci.html

-- load module / file
-- :load File
-- :l File
-- :reload

-- display information about the given names
-- :info name
-- :i name

-- print type after evaluation
-- :set +t

-- shows type of an expression
-- :t []

-- The factorial function

-- Variant 1 (pattern matching)
-- fact1 :: (Num a, Eq a) => a -> a
fact1 0 = 1
fact1 n = n * (fact1 (n-1))

-- Variant 2 (conditional expressions)
-- fact2 :: (Num a, Eq a) => a -> a
fact2 n = if (n == 0) then 1
		    else (n * (fact2 (n-1)))

-- Variant 3 (guarded equations)
-- fact3 :: (Num a, Eq a) => a -> a
fact3 n | n == 0	= 1
		| otherwise	= n * (fact2 (n-1))


-- The nth Fibbonacci number

-- Variant 1
-- fib1 :: (Num a1, Num a, Eq a) => a -> a1
fib1 1 = 1
fib1 2 = 1
fib1 n = (fib1 (n-1)) + (fib1 (n-2))

-- Variant 2
-- fib2 :: (Num a1, Num a, Eq a1) => a1 -> a
fib2 n = 	if (n == 1) then 1
			else if (n == 2) then 1
			else (fib2 (n-1)) + (fib2 (n-2))

-- Variant 3
-- fib3 :: (Num a1, Num a, Eq a1) => a1 -> a
fib3 n 	| n == 1 	= 1
		| n == 2 	= 1
		| otherwise	= (fib3 (n-1)) + (fib3 (n-2))



-- Starting to dream
-- The first n elements of the Fibonacci series
-- nfibs :: (Num b, Num a, Eq a, Enum a) => a -> [b]
nfibs n = map fib1 [1..n]

-- Getting totally crazy
-- The Fibonacci series itself
-- fibs1 :: [Integer]
fibs1 = map fib1 [1..]

-- Or even more cyyptic, but more efficient
-- fibs2 :: [Integer]
fibs2 = 0 : 1 : zipWith (+) fibs2 (tail fibs2)

-- Trying to get the value of fibs1 or fibs2 does not terminate, as expected

-- What does terminate:
-- take 6 fibs1
-- take 6 fibs2

-- Algebraic Data Types

-- LISTS

data Lst a = Nil | Cons a (Lst a) deriving (Show)

id x = x

l1 = Cons 'a' (Cons 'b' Nil)
l2 = Cons 'c' (Cons 'd' Nil)
l3 = Cons 1 (Cons 2 Nil)
l4 = Cons True (Cons False Nil)
-- :t l1

-- len :: List a -> Int
-- len :: Num a => List t -> a
len Nil = 0
len (Cons _ tl) = 1 + len tl

-- :t len
-- len l1 == 2

-- mem :: Eq a => a -> Lst a -> Bool
mem e Nil = False
mem e (Cons hd tl) = (e == hd) || (mem e tl)
-- Alternative
-- mem e (Cons hd tl) = if (e == hd) then True else (mem e tl)


-- app :: Lst a -> Lst a -> Lst a
app Nil ls = ls
app (Cons hd tl) ls = Cons hd (app tl ls)


-- total :: Num a => Lst a -> a
total Nil = 0
total (Cons hd tl) = hd + (total tl)
-- total l3 == 3
-- total l2

-- prod :: Num a => Lst a -> a
prod Nil = 1
prod (Cons hd tl) = hd * (prod tl)

-- prod l3 == 2

-- disj :: Lst Bool -> Bool
disj Nil = True
disj (Cons hd tl) = hd || (disj tl)
-- disj l4 == True

-- conj :: Lst Bool -> Bool
conj Nil = False
conj (Cons hd tl) = hd || (conj tl)
-- disj l4 == True

showLst Nil = "[]"
showLst (Cons hd tl) = "[" ++ (show hd) ++ (showLst' tl) ++ "]"
	-- Indentation important!
	where
		showLst' Nil = ""
		showLst' (Cons hd tl) = ", " ++ (show hd) ++ (showLst' tl)

-- :t showLst

-- Higher order functions

-- mapp :: (t -> a) -> Lst t -> Lst a
mapp f Nil = Nil
mapp f (Cons hd tl) = Cons (f hd) (mapp f tl)

increment = mapp (\x -> x + 1)
double = mapp (\x -> x * 2)
square = mapp (\x -> x * x)

-- fltr :: (a -> Bool) -> Lst a -> Lst a
fltr f Nil = Nil
-- fltr f (Cons hd tl) = if (f hd) then (Cons hd (fltr f tl)) else (fltr f tl)
-- Alternative in 'guard' style
fltr f (Cons hd tl)
	| (f hd)    = (Cons hd (fltr f tl))
	| otherwise = (fltr f tl)

-- delete :: Lst a -> Lst a
delete = fltr (\x -> False)
-- onlyAs :: Lst Char -> Lst Char
onlyAs = fltr (\x -> x == 'a')

-- partition :: (a -> Bool) -> Lst a -> (Lst a, Lst a)
partition f ls = (fltr f ls, fltr (\x -> not (f x)) ls)

-- reduce :: (t -> t1 -> t1) -> t1 -> Lst t -> t1
reduce f i Nil = i
reduce f i (Cons hd tl) = f hd (reduce f i tl)

-- Operations that can be redefined in terms of reduce
redTotal = reduce (\x y -> x + y) 0
redProd = reduce (*) 1
redDisj = reduce (||) False
redConj = reduce (&&) True
redApp l1 l2 = reduce Cons l2 l1


insert e Nil = (Cons e Nil)
insert e (Cons hd tl)
	| e < hd    = (Cons e (Cons hd tl))
	| otherwise = (Cons hd (insert e tl))

-- insertionSort :: Ord a => Lst a -> Lst a
insertionSort ls = insertionSortHelper Nil ls
	where
		-- First argument is sorted, second is to be sorted
		insertionSortHelper sorted Nil = sorted
		insertionSortHelper sorted (Cons hd tl) = insertionSortHelper (insert hd sorted) tl

-- quicksort :: Ord a => Lst a -> Lst a
quicksort Nil = Nil
quicksort (Cons hd tl) = app (quicksort smaller) (Cons hd (quicksort larger))
	where
		(smaller, larger) = (partition (\x -> x < hd) tl)


-- BINARY TREES

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)


t1 = Node 2 (Node 1 EmptyTree EmptyTree) (Node 3 EmptyTree EmptyTree)

treeMap f EmptyTree = EmptyTree
treeMap f (Node a left right) = Node (f a) (treeMap f left) (treeMap f right)

-- repTree :: (Ord a, Num a) => a1 -> a -> Tree a1
repTree e depth 
	| depth <= 0 = EmptyTree
	| otherwise  = Node e (repTree e (depth-1)) (repTree e (depth-1))

-- preorder :: Tree t -> [t]
preorder EmptyTree = []
preorder (Node e left right) = [e] ++ (preorder left) ++ (preorder right)

-- inorder :: Tree t -> [t]
inorder EmptyTree = []
inorder (Node e left right) = (inorder left) ++ [e] ++ (inorder right)

-- postorder :: Tree t -> [t]
postorder EmptyTree = []
postorder (Node e left right) = (postorder left) ++ (postorder right) ++ [e]

-- singleton :: a -> Tree a  
singleton e = Node e EmptyTree EmptyTree  
  
-- treeInsert :: (Ord a) => a -> Tree a -> Tree a  
treeInsert x EmptyTree = singleton x  
treeInsert x (Node a left right)   
    | x == a = Node x left right  
    | x < a  = Node a (treeInsert x left) right  
    | x > a  = Node a left (treeInsert x right)  

-- treeSearch :: (Ord a) => a -> Tree a -> Bool  
treeSearch x EmptyTree = False  
treeSearch x (Node a left right)  
    | x == a = True  
    | x < a  = treeSearch x left  
    | x > a  = treeSearch x right

treeSort ls = inorder (foldr treeInsert EmptyTree ls)


